from mysql.connector import connect, cursor

class database:
    def __init__(self):
        try:
            self.db = connect(host='localhost', database='perpustakaan', user='root', password='')
        except Exception as e:
            print(e)
    
    def showUserByEmail(self, **params):
        cursor = self.db.cursor()
        query = '''select customer.username,borrow.* from borrow inner join customer on borrow.userid = customer.userid where customer.email = {0} and borrow.isactive = 1'''.format(params["email"])

        cursor.execute(query)
        result = cursor.fetchone()
        return result
    
    def insertBorrow(self):
        column = ', '.join(list(params.keys()))
        values = tuple(list(params.values()))
        cursor = self.db.cursor()
        query = '''insert into ({0}) values {1}'''.format(column, values)

        cursor.execute(query)
    
    def updateBorrow(self):
        borrowid = params['borrowid']
        cursor = self.db.cursor()
        query = '''update borrow set isactive = 0 where borrowid = {0}'''.format(borrowid)

        cursor.execute(query)

    def dataCommit(self):
        self.db.commit()
