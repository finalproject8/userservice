from mysql.connector.dbapi import DateFromTicks
from app.models.borrow_model import database
from app.models.customer_model import database as cust_db
from flask import jsonify, request
from flask_jwt_extended import *
import json, datetime

mysqldb = database()

@jwt_required()
def shows():
    params = get_jwt_identity()
    dbresult = mysqldb.showBorrowByEmail(**params)
    result = []
    if dbresult is not None:
        for items in dbresult:
            id = json.dumps({"id" : items[4]})
            bookdetail = getBookById(id)
            user = {
                "username" : items[0],
                "borrowid" : items[1],
                "borrowdate" : items[2],
                "bookid" : items[3],
                "bookname" : items[4],
                "author" : bookdetail["pengarang"],
                "releaseyear" : bookdetail["tahunterbit"],
                "genre" : bookdetail["genre"],
            }
            result.append(user)
        else:
            result= dbresult
    return jsonify(result)
@jwt_required()
def insert():
    token = get_jwt_identity()
    userid = cust_db.showUserByEmail(**token)[0]
    borrowdate = datetime.datetime.now().isoformat()
    id = json.dumps({"id":params["bookid"]})
    bookname = getBookById(id)["nama"]
    params.update(
        {
            "userid" : userid,
            "borrowdate" : borrowdate,
            "bookname"   : bookname,
            "isactive"   : 1
        }
    )
    mysqldb.insertBorrow(**params)
    mysqldb.dataCommit()
    return jsonify({"message":"success"})

@jwt_required()
def changeStatus(**params):
    mysqldb.updateBorrow(**params)
    mysqldb.dataCommit()
    return jsonify({"message":"success"})

def getBookById():
    book_data = request.get(url="http://localhost:5000/bookbyid", data=data)
    return book_data.json()